import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styles: [`
    .white { color: white }
  `]
})
export class DetailsComponent implements OnInit {
  showParagraph = false;
  clicks = [];
  detail = 0;
  constructor() { }

  ngOnInit() {
  }

  onClick() {
    this.showParagraph = !this.showParagraph;
    this.detail = Date.now();
    this.clicks.push(this.detail);
  }

  getColor(index) {
    return index >= 4 ? 'blue' : 'white';
  }
}
